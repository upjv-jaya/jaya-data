INSERT INTO "SchoolYear"(id, label) VALUES
  (1, 'M1'),
  (2, 'M2');
ALTER SEQUENCE "SchoolYear_id_seq" RESTART WITH 3;

-- -----
-- Email et mot de passe (crypté et généré par le script) à remplacer avec les valeurs voulues
-- -----
INSERT INTO "Credentials"(id, email, password, is_verified) VALUES
  (1, 'gwenaelle.berquin@u-picardie.fr', '$2a$10$LW50CqVIzm8eO/bDZFbtxeRK8DEKzSSSlAL/v/gXoP4nzXOL8.QqC', TRUE);

INSERT INTO "Admin"(id, credentials_id) VALUES
  (1, 1);