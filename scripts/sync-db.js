// Si on utilise les variable d'environnement définies plus bas, commenter cette ligne
import '../setup.local'
import { setUpDatabase } from '../src/utils/database'

// Décommenter ces lignes pour remplacer par les infos correctes
// process.env.DB_HOST = 'jayadb.caihpgpe4j0x.eu-west-2.rds.amazonaws.com'
// process.env.DB_USER = 'master'
// process.env.DB_PASSWORD = 'mastercraft'
// process.env.DB_NAME = 'jaya_database'

const Sequelize = require('../src/db').default

console.log(`=> Trying to sync the database (${process.env.DB_NAME}) with new models`)
console.log('==============================================')
console.log('')

setUpDatabase(Sequelize, true)
  .then(({ orm }) => orm.sync())
  .then(() => {
    console.log('=> Sync done.')
    process.exit(0)
  })
  .catch((e) => {
    console.error('/!\\ Cannot perform sync operation :(...')
    if (e) {
      console.error(e.message)
    }
    process.exit(1)
  })
