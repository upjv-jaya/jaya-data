import Sequelize from 'sequelize'

console.log(process.env.DB_HOST)
const dbURI =
  `postgres://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:5432/${process.env.DB_NAME}`
const initializedSequelize = new Sequelize(dbURI, { logging: false })

export default initializedSequelize
