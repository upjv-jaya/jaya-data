export default models => ({
  addProfessorSubject: (_, { professorId, subjectId, lectureHours, tutorialHours }) =>
    models.ProfessorSubject.create({
      professor_id: professorId,
      subject_id: subjectId,
      tutorialHoursPerGroup: tutorialHours,
      lectureHours: lectureHours
    }, {
      include: [{
        all: true
      }],
      returning: true
    }),
  updateProfessorSubject: async (_, { id, professorId, subjectId, lectureHours, tutorialHours }) => {
    const professorSubject = await models.ProfessorSubject.findById(id, {
      include: [{ all: true }]
    })

    return professorSubject.update({
      professor_id: professorId,
      subject_id: subjectId,
      tutorialHoursPerGroup: tutorialHours,
      lectureHours: lectureHours
    })
  },
  deleteProfessorSubject: async (_, { id }) => {
    const nbDeleted = await models.ProfessorSubject.destroy({
      where: { id }
    })

    if (nbDeleted === 1) {
      return { dataId: id, entity: 'ProfessorSubject' }
    }

    throw new Error('Cannot delete entity')
  }
})
