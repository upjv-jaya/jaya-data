const ProfessorSubjectQueries = `
 extend type Query {
  allProfessorSubjects: [ProfessorSubject]
  professorSubjectById (id: ID!): ProfessorSubject
  professorSubjectsByProfessorId (id: ID!): [ProfessorSubject]
 }
`

export default () => [ProfessorSubjectQueries]
