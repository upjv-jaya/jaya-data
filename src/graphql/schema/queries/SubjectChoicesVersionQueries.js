const SubjectChoicesVersionQueries = `
extend type Query {
  allSubjectChoicesVersions: [SubjectChoicesVersion]
  subjectChoicesVersionById(id: ID!): SubjectChoicesVersion
  subjectChoicesVersionsBySchoolYearAndSemester(
    schoolYearId: ID!
    semester: Semester
  ): [SubjectChoicesVersion]
  activesSubjectChoicesVersionsBySchoolYearForStudent(
    schoolYearId: ID!
    studentId: ID!
  ): [SubjectChoicesVersion]
}
`

export default () => [SubjectChoicesVersionQueries]
