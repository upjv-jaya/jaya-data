const ProfessorSubjectMutations = `
  extend type Mutation {
    addProfessorSubject (
      professorId: ID!
      subjectId: ID!
      lectureHours: Int!
      tutorialHours: Int!
    ): ProfessorSubject
    
    updateProfessorSubject (
      id: ID!,
      professorId: ID!
      subjectId: ID!
      lectureHours: Int!
      tutorialHours: Int!
    ): ProfessorSubject
    
    deleteProfessorSubject (id: ID!): DeletedData
  }
`

export default () => [ProfessorSubjectMutations]
