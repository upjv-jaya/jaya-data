export default models => ({
  allProfessorSubjects: () => models.ProfessorSubject.findAll({
    include: [{
      all: true
    }]
  }),
  professorSubjectById: (_, { id }) => models.ProfessorSubject.findById(id, {
    include: [{
      all: true
    }]
  }),
  professorSubjectsByProfessorId: (_, { id }) => models.ProfessorSubject.findAll({
    where: { professor_id: id },
    include: [{
      all: true
    }]
  })
})
